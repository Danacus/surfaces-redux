---------------------------------------------------------------------------------------------------
Version: 1.1.9
  Fixes:
    - Fixed crash with Angel's industry due to poorly implemented check for
      the presence of Bob's electronics.
---------------------------------------------------------------------------------------------------
Version: 1.1.8
  Changes:
    - Significantly reduced cave wall health to make grenades (and nukes) a
      more feasible way to clear large areas. Robots can also be used, but
      Construction Drones are broken for unknown reasons.
  Fixes:
    - Fixed issue where the player character disappears when the surface gets
      generated.
---------------------------------------------------------------------------------------------------
Version: 1.1.7
  Fixes:
    - Fixed missing icon_size property (by Thaui)
---------------------------------------------------------------------------------------------------
Version: 1.1.6
  Info:
    - Factorio version 1.1
---------------------------------------------------------------------------------------------------
Version: 1.1.5
  Fixes:
    - Fixed desync caused by seemingly useless pause_check variable used to
      skip events during first tick (which can cause desyncs).
---------------------------------------------------------------------------------------------------
Version: 1.1.4
  Fixes:
    - Listen to script_raised events
---------------------------------------------------------------------------------------------------
Version: 1.1.3
  Fixes:
    - Added Alien Biomes hack to prioritize tiles and prevent removal
---------------------------------------------------------------------------------------------------
Version: 1.1.2
  Fixes:
    - Fixed potential issue with sprite icons (hopefully)
---------------------------------------------------------------------------------------------------
Version: 1.1.1
  Changes:
    - Listen for script_raised_destroy for when walls are destroyed by another 
      mod.
---------------------------------------------------------------------------------------------------
Version: 1.1.0
  Info:
    - Updated to 0.18
---------------------------------------------------------------------------------------------------
Version: 1.0.3
  Features:
    - Alien Biomes support
---------------------------------------------------------------------------------------------------
Version: 1.0.2
  Features:
    - The main surface for this mod can now be changed in the mod settings.
---------------------------------------------------------------------------------------------------
Version: 1.0.1
  Fixes:
    - Fixes mod not loading after recent name change.
---------------------------------------------------------------------------------------------------
Version: 1.0.0
  Info:
    - First functional port to 0.17
  Changes:
    - You are no longer able to create a shaft when it conflicts with another
      entity on the target surface. This prevents shafts from being created
      inside of your assemblers.
  Fixes:
    - Fixed Map Generation Settings type checking to work with the changes
      introduced in 0.17. 
