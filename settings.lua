data:extend({
    {
        type = "string-setting",
        name = "main-surface",
        setting_type = "runtime-global",
        default_value = "nauvis",
        localised_name = "Main surface",
        localised_description = "Sets the main surface used by the mod."
    }
})